import React from 'react';

const Title = ({type, theme, classname, text}) => {
    const TitleTag = type;

    return (
        <TitleTag className={`title title--${type} ${theme && `title--${theme}`} ${classname && classname}`}>
            {text}
        </TitleTag>
    )
};

export default Title;