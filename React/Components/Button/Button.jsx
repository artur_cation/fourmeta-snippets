import React from 'react';

import './Button.scss';

const Button = ({typeButton, classname, buttonText, disabled}) => {

    let type = typeButton || 'button';

    const handleClick = (e) => {

    };

    return (
        <button
            type={type}
            className={`button ${classname}`}
            disabled={disabled}
            onClick={handleClick}
        >
            <span className="button__text">{buttonText}</span>
        </button>
    )
};

export default Button;
