/** Check Object on Equals
 @function
 * @param {Object} x
 * @param {Object} y
 * @return Boolean
 **/
export const isEquals = (x, y) => {
    if (x === y) return true;
    if (!(x instanceof Object) || !(y instanceof Object)) return false;
    if (x.constructor !== y.constructor) return false;
    for (let p in x) {
        if (!x.hasOwnProperty(p)) continue;
        if (!y.hasOwnProperty(p)) return false;
        if (x[p] === y[p]) continue;
        if (typeof (x[p]) !== "object") return false;
        if (!isEquals(x[p], y[p])) return false;
    }
    for (let p in y) {
        if (y.hasOwnProperty(p) && !x.hasOwnProperty(p)) return false;
    }
    return true;
};

/** Remove item from Array
 @function
 * @param {Array} arr
 * @param {String | Number | Object} value
 * @return {Array}
 **/
export const removeArrayItem = (arr, value) => {
    let b = '';
    for (b in arr) {
        if (arr[b] === value) {
            arr.splice(b, 1);
            break;
        }
    }
    return arr;
};

/** Get URL Params
 @function
 * @param {String} q
 * @return {String}
 **/
export const getQuery = (q) => ((typeof window !== 'undefined' ? window.location.search : '').match(new RegExp('[?&]' + q + '=([^&]+)')) || [, null])[1];


/** Set Cookie
 @function
 * @param {String} cname
 * @param {String} cvalue
 * @param {Number} exdays
 **/
export const setCookie = (cname, cvalue, exdays) => {
    let d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
};


/** Get Cookie
 @function
 * @param {String} cname
 * @return {String}
 **/

export const getCookie = (cname) => {
    let name = cname + "=";
    let ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1)
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length)
        }
    }
    return "";
};

/** Check object on empty
 @function
 * @param {Object} obj
 * @return {Boolean}
 **/

export const isEmptyObj = (obj) => {
    if (obj == null) return true;
    if (obj.length > 0) return false;
    if (obj.length === 0) return true;
    for (let key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }
    return true;
}

/**
 * Generate an unique token
 @function
 * @return {string}
 */
export const uuid = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        let r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
};