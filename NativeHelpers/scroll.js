export const preventDefault = e => {
    e.preventDefault();
    return false;
};

export const disableScroll = level => {
    if (level === 'soft') {
        window.addEventListener('wheel', preventDefault, {passive: false});
        window.addEventListener('scroll', preventDefault, {passive: false});
    } else if ('super-soft') {
        document.body.classList.add('no-scroll');
        document.documentElement.classList.add('no-scroll');
    } else {
        const x=window.scrollX;
        const y=window.scrollY;
        window.onscroll=function(){window.scrollTo(x, y);};
        window.addEventListener('wheel', preventDefault, {passive: false});
        window.addEventListener('scroll', preventDefault, {passive: false});
        window.addEventListener('touchmove', preventDefault, {passive: false});
        window.addEventListener('touchstart', preventDefault, {passive: false});
    }
    document.body.classList.add('no-scroll');
    document.documentElement.classList.add('no-scroll');
};

export const enableScroll = level => {
    if (level === 'soft') {
        window.removeEventListener('wheel', preventDefault, {passive: false});
        window.removeEventListener('scroll', preventDefault, {passive: false});
    } else if ('super-soft') {
        document.body.classList.remove('no-scroll');
        document.documentElement.classList.remove('no-scroll');
    } else {
        window.onscroll=function(){};
        window.removeEventListener('wheel', preventDefault, {passive: false});
        window.removeEventListener('scroll', preventDefault, {passive: false});
        window.removeEventListener('touchmove', preventDefault, {passive: false});
        window.removeEventListener('touchstart', preventDefault, {passive: false});
    }
    document.body.classList.remove('no-scroll');
    document.documentElement.classList.remove('no-scroll');
};

export const goTop = () => {
    window.scrollTo({
        top: 0,
        behavior: "smooth"
    })
};