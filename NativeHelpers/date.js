/** this function return user age **/
export const calculateAge = birth => {
    let dateOfBirth = new Date(birth);
    let todaysDate = new Date();

    let years = todaysDate.getFullYear() - dateOfBirth.getFullYear();
    let months = (todaysDate.getMonth() - dateOfBirth.getMonth());

    return ((months < 0) || (months === 0 && (todaysDate.getDay() - dateOfBirth.getDay()) < 0)) ? --years : years;
}

/** this function return true if date is in the future **/
export const isFutureDate = date => new Date(date) > new Date();

/** this function return true if date is in the past **/
export const isPastDate = date => new Date(date) < new Date();

/** this function returns pretty format of the date: November 1, 2020, 09:44:42 PM **/
export const toDate = date => {
    return new Intl.DateTimeFormat('en-EN', {
        day: '2-digit',
        month: 'long',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit'
    }).format(new Date(date))
};

/** this function returns pretty format of the price **/
export const toCurrency = price => {
    return new Intl.NumberFormat('ru-RU', {
        currency: 'rub',
        style: 'currency'
    }).format(price)
};
